## Kibana chart
helm install kibana elastic/kibana -f kibana.yaml -n logging

## elasticsearch chart
helm install elasticsearch elastic/elasticsearch -f elasticsearch.yaml -f logging

## kube-prometheus-stack chart
helm install prometheus prometheus-community/kube-prometheus-stack -f prometheus-stack.yaml

